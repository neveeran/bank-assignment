import { Component } from '@angular/core';
import { Papa } from 'ngx-papaparse';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rabo-proj';
  constructor(private papaParse: Papa) {}
  public failedRecords = [];
  public accountDetailsFormat  = {
    accountnumber: '',
    description: '',
    endbalance: '',
    mutation: '',
    reference: '',
    startbalance: ''
   };
  // Function called when there is a file upload.
  parse(files: FileList): void {
    this.failedRecords = [];
    const file: File = files.item(0);
    const reader: FileReader = new FileReader();
    reader.readAsText(file);
    reader.onload = e => {
      if (file.type === 'text/csv') {
        const csv: string = reader.result as string;
        const parsed = this.papaParse.parse(csv, {
          header: true,
          quoteChar: '"',
          escapeChar: '"',
          skipEmptyLines: true
        });
        this.validateCSVData(parsed.data);
      }
    };
}

// Check for transaction reference if they are any duplicates and if any those are moved to failed records to be displayed.
checkUniqueTransactionRef(data) {
  const calculateReference = {};
  const unique = data.filter((v, i) => {
    const referenceCheck = v.Reference || v.reference;
    if (referenceCheck in calculateReference) {
      this.formatFailedRecords(data[i]);
      return false;
    } else {
      calculateReference[referenceCheck] = true;
      return true;
    }
  });
  return unique;
}

// Validate data from csv this is to construct the data to desired account details format.
validateCSVData(data) {
  // This is to avoid typescript error of using string literal in array.
  const mutation = 'Mutation';
  const reference = 'Reference';
  const description = 'Description';
  const uniqueTransactionReference = this.checkUniqueTransactionRef(data);
  for (const details of uniqueTransactionReference) {
    this.accountDetailsFormat.startbalance = details['Start Balance'];
    this.accountDetailsFormat.endbalance = details['End Balance'];
    this.accountDetailsFormat.mutation = details[mutation];
    this.accountDetailsFormat.reference = details[reference];
    this.accountDetailsFormat.description = details[description];
    this.calculateBalance(this.accountDetailsFormat);
  }
}

// Validate data from xml this is to construct the data to desired account details format.
validateXMLData(data) {
  if (data && data.records.record.length > 1) {
     const unique = this.checkUniqueTransactionRef(data.records.record);
     for (const details of unique) {
      this.accountDetailsFormat.startbalance = details.startbalance;
      this.accountDetailsFormat.endbalance = details.endbalance;
      this.accountDetailsFormat.mutation = details.mutation;
      this.accountDetailsFormat.reference = details.reference;
      this.accountDetailsFormat.description = details.description;
      this.calculateBalance(this.accountDetailsFormat);
     }
   } else {
      const details = data.records.record;
      this.accountDetailsFormat.startbalance = details.startbalance;
      this.accountDetailsFormat.endbalance = details.endbalance;
      this.accountDetailsFormat.mutation = details.mutation;
      this.accountDetailsFormat.reference = details.reference;
      this.accountDetailsFormat.description = details.description;
      this.calculateBalance(this.accountDetailsFormat);
   }
  }

// Used to calculate balance based on mutation value.
calculateBalance(data) {
  // search for + or - in the mutation value.
  const addMutation = data.mutation.includes('+');
  const minusMutation = data.mutation.includes('-');
  if (addMutation) {
    const addBalance = data.mutation.split('+')[1];
    const amount = (parseFloat(data.startbalance) + parseFloat(addBalance));
    this.validateBalance(data, amount, parseFloat(data.endbalance));
  } else if (minusMutation) {
    const minusBalance = data.mutation.split('-')[1];
    const amount = (parseFloat(data.startbalance) - parseFloat(minusBalance));
    this.validateBalance(data, amount, parseFloat(data.endbalance));
  }
}


// Validate balance - Compares amount after mutation and end balance and process data accordingly.
validateBalance(data, amount, balance) {
  const amountRound = Math.round(amount * 100) / 100;
  const balanceRound = Math.round(balance * 100) / 100;
  if (amountRound === balanceRound) {
    return true;
  } else {
    this.formatFailedRecords(data);
    return false;
  }
}

// Format failed records these are the fields required to show the user so need to construct array with them based on details received.
formatFailedRecords(data) {
  // This is to avoid typescript error of using string literal in array.
  const reference = 'Reference';
  const description = 'Description';
  const failedFormat = {
      reference : data[reference] || data.reference,
      description : data.description || data[description]
  };
  this.failedRecords.push(failedFormat);
}
}
